import React, { Component } from 'react';

//importamos el menu para mostrarlo aca
import Main from './components/MainComponent';
import './App.css';
import {BrowserRouter } from 'react-router-dom';


//REDUX
//introduccion a redux: https://redux.js.org/introduction/getting-started
//provider= permite configurar mi app React para que Redux store este disponible en todos los componentes de la app
import { Provider } from 'react-redux';
import {ConfigureStore} from './redux/configureStore'

//de este modo disponibilizamos el store en toda la app
//primero creo la variable y luego envio a mis componenetes como parametro en <Provider store={store}>  
const store = ConfigureStore();

class App extends Component {

  

  render() {
    return(
     <Provider store={store}>  
        <BrowserRouter> 
          <div>          
            <Main/>      
          </div>
        </BrowserRouter>
      </Provider>
    )
  };
}

export default App;
