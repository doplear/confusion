import React, { Component } from 'react';
//dirigir navegacion
import { Switch, Route, Redirect, withRouter} from 'react-router-dom';
//animations
import {TransitionGroup,CSSTransition} from 'react-transition-group'

//REDUX: con redux usamos withRouter de react-router-dom y tambien connect
import {connect} from 'react-redux';
//importamos la definicion de addComent que creamos
import {postComment, fetchDishes, fetchComments,fetchPromos, fetchLiders,postFeedback} from '../redux/ActionCreators';
//utilizado con los forms
import {actions} from 'react-redux-form'


//components
import Menu from './MenuComponent';
import DishDetail from './DishdetailComponent';
import Header from './HeaderComponent';
import Footer from './FooterComponent';
import Home from './HomeComponent';
import Contact from './ContactComponent';
import About from './AboutComponent';


 //onbtiene el estado como parametro. El state lo esta recibiendo de app.js desde Provider
 const mapStateToProps = state =>{
   return{
      dishes:     state.dishes,
      comments:   state.comments,
      promotions: state.promotions,
      leaders:    state.leaders
   }
    
}
//esta funcion ejecutara la accion que crea addComment: que pasaremos  como props a DishDetail mas abajo
//fetchDishes es un thunk definido en ../redux/ActionCreators
//feedback va a ser el nombre del form
const mapDispactchToProps=(dispatch) =>({
  
  postComment:(dishId,rating,author,comments)=> dispatch(postComment(dishId,rating,author,comments)),
  fetchDishes:() => {dispatch(fetchDishes())},
  resetFeedbackForm:() => {dispatch(actions.reset('feedback'))},
  fetchComments:() => {dispatch(fetchComments())},
  fetchPromos:() => {dispatch(fetchPromos())},
  fetchLiders:() =>{dispatch(fetchLiders())},  
  postFeedback:(firstname,lastname,telnum,email,agree,contactType,message)=> dispatch(postFeedback(firstname,lastname,telnum,email,agree,contactType,message))
 

});

class Main extends Component {

  


//componentDidMount metodo del ciclo de vida del componente. Lo que ponga ahi se ejecutara cuando el componente se monte
//en la vista de la app
//cuando el Main component se monte llamara al metodo fetchDishes
  componentDidMount(){
    this.props.fetchDishes()
    this.props.fetchComments()
    this.props.fetchPromos()
    this.props.fetchLiders()
    

  }

 

//exact path quiere decir que el path tiene que ser exactamente igual a Menu sin que haya nada mas alla
//component={() => <Menu dishes={this.state.dishes }= paso informacion al componente Menu via props
//<Redirect to="/home"/> = funciona como una especie de else, por defecto si no se coincide con ninguno de los paths de arriba redirige el trafico a home
//this.props.dishes.dishes la ifnormacion ahora se encuentra aca porque esta yendo a buscar a redux/dishes y ahi esta la propiedad dishes:[]
  render() {
     const HomePage = ()=>{
       return(
         <Home dish={this.props.dishes.dishes.filter((dish) => dish.featured)[0]}
               dishesLoading={this.props.dishes.isLoading}
               dishesErrMess={this.props.dishes.errMess}
               promotion={this.props.promotions.promotions.filter((promo) => promo.featured)[0]}
               promosLoading={this.props.promotions.isLoading}
               promosErrMess={this.props.promotions.errMess}
               leader={this.props.leaders.leaders.filter((leader) => leader.featured)[0]}  
               leaderLoading={this.props.leaders.isLoading}
               leaderErrMess={this.props.leaders.errMess}  

         />
       );
     }
//match, location, history esos tres parametros reciben siempre cuando se envia desde Route, pero solo nos interesa match
//addComment= es la funcion creada arriba en  mapDispactchToProps   
  const DishWithId = ({match}) =>{

        return(
       
          <DishDetail dish={this.props.dishes.dishes.filter((dish) => dish.id === parseInt(match.params.dishId,10))[0] }
            isLoading={this.props.dishes.isLoading}
            errMess={this.props.dishes.errMess}
            comments={this.props.comments.comments.filter((comment)=> comment.dishId === parseInt(match.params.dishId,10))}  
            commentsErrMess={this.props.comments.errMess}          
            postComment={this.props.postComment}/>  
            
        )
     }
//</TransitionGroup>= donde estoy aplicando las animaciones
//cada componente recibe tres parametros match,location y key aca usamos key
//postFeedback={this.props.postFeedback}= con esto paso por props la funcion postFeedback() que tiene el actionCreator
    return(
    <div >
      <Header /> 
      <TransitionGroup>
          <CSSTransition key={this.props.location.key} classNames="page" timeout={300}>
            <Switch>
                <Route path="/home" component={HomePage}></Route>          
                <Route exact path="/menu" component={() => <Menu dishes={this.props.dishes } /> }></Route>
                <Route path="/menu/:dishId" component={DishWithId } /> }/>
                <Route exact path="/contactus" component={()=> <Contact 
                                                                    resetFeedbackForm={this.props.resetFeedbackForm}
                                                                    postFeedback={this.props.postFeedback}/>}/> 
                <Route exact path="/aboutus" component={() => <About leaders={this.props.leaders.leaders } 
                                                                     isLoading={this.props.leaders.isLoading}
                                                                     errMess={this.props.leaders.errMess} 
                                                              /> }></Route>
                <Redirect to="/home"/>
            </Switch>
          </CSSTransition>
       </TransitionGroup>
      <Footer/>
      
    </div>
    )
  };
}


//withRouter(connect(mapStateToProps)(Main)); hacemos esto para conectar los componentes con Redux store
export default withRouter(connect(mapStateToProps, mapDispactchToProps)(Main));
