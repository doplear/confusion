import React,{Component} from 'react';
import {Navbar, NavbarBrand,Nav, NavbarToggler,Collapse,NavItem,Jumbotron, 
Button,Modal,ModalHeader,ModalBody, Form, FormGroup, Label, Input} from 'reactstrap'
import {NavLink} from 'react-router-dom';


//<NavLink className="nav-link" to="/home">= Asi usamos NavLink de react-router-dom para redirigir el trafico
// expand="md" con esto indicamos que vamos a mostrar el menu solamente para pantallas medianas o extra grandes
//<Collapse> = usamos collapse  para las pantallas chicas 
// <NavBarToggler onClick={this.toggleNav} /> solo esta activo para pantallas chicas
//isOpen={this.state.isNavOpen} si esto es falso todo lo que esta dentro del collapse se esconde. NavBarToggler cambiara el estado de true a false y viceversa
// this.toggleNav = this.toggleNav.bind(this) = Declaramos dentro del constructor para luego poder usarlo dentrho de la creacion de los links como this.toggleNav
//isModalOpen=utilizado para la ventana Modal para saber si esta abierto o no 
//<Button outline onClick={this.toggleModal}>= el boton que llama a la ventana modal
class Header extends Component{

constructor(props){
    super(props);
    this.state={
        isNavOpen:false,
        isModalOpen:false

    }
        this.toggleNav =   this.toggleNav.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    toggleNav(){     
        this.setState({
            isNavOpen:!this.state.isNavOpen
        });
    }

    toggleModal(){
        this.setState({
            isModalOpen:!this.state.isModalOpen
        });
    }

    handleLogin(event){
       //con esto accedemos al valor directamente del DOM
       //uncontroled forms
        this.toggleModal();
        alert("Username: " + this.username.value + " Password: "+ this.password.value + " Remember: " + this.remember.checked )
        event.preventDefault();
    }



   render(){
        return(

            <React.Fragment>
                <Navbar dark  expand="md">
                    <div className="container">
                        <NavbarToggler onClick={this.toggleNav} />
                        <NavbarBrand className="mr-auto" href="/">
                            <img src='/assets/images/logo.png' height="30" width="42" 
                            alt="Ristorante Con Fusion"></img>
                        </NavbarBrand>
                        <Collapse isOpen={this.state.isNavOpen} navbar>
                            <Nav navbar>
                                <NavItem>
                                    <NavLink className="nav-link" to="/home">
                                        <span className="fa fa-home fa-lg"></span> Home
                                    </NavLink>
                                </NavItem>

                                <NavItem>
                                    <NavLink className="nav-link" to="/aboutus">
                                        <span className="fa fa-info fa-lg"></span> About Us
                                    </NavLink>
                                </NavItem>

                                <NavItem>
                                    <NavLink className="nav-link" to="/menu">
                                        <span className="fa fa-list fa-lg"></span> Menu
                                    </NavLink>
                                </NavItem>

                                <NavItem>
                                    <NavLink className="nav-link" to="/contactus">
                                        <span className="fa fa-address-card fa-lg"></span> Contact Us
                                    </NavLink>
                                </NavItem>
                            </Nav> 
                            <Nav className="ml-auto" navbar>
                                <NavItem>
                                    <Button outline onClick={this.toggleModal}>
                                        <span className="fa fa-sign-in fa-lg"></span> Login
                                    </Button>
                                </NavItem>
                            </Nav>  
                        </Collapse>                      
                    </div>
                </Navbar>  
                <Jumbotron>
                    <div className="container">
                        <div className = "row row-header">
                            <div className= "col-12 col-sm-6">
                                <h1>Ristorante Con Fusion</h1>
                                <p>We take inspiration from the World's best cuisines, and create a unique fusion experience. Our lipsmacking creations will tickle your culinary senses!</p>
                            </div>
                        </div>
                    </div>
                </Jumbotron>  
                <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
                        <ModalHeader toggle={this.toggleModal}>Login</ModalHeader>
                        <ModalBody>
                            <Form onSubmit={this.handleLogin}>
                                <FormGroup>
                                    <Label htmlFor="username">Username</Label>
                                    <Input type="text" id="username" name="username"
                                           innerRef={(input) => this.username = input} >
                                    </Input>                                    
                                </FormGroup>
                                <FormGroup>
                                    <Label htmlFor="password">Password</Label>
                                    <Input type="password" id="password" name="password"
                                            innerRef={(input) => this.password = input}>
                                    </Input>                                    
                                </FormGroup>
                                <FormGroup check>
                                    <Label check>
                                        <Input type="checkbox" name="remember"
                                               innerRef={(input) => this.remember = input}>
                                        </Input> Remember me
                                    </Label>
                                </FormGroup>

                                <Button type="submit" value="submit" color="primary">Login</Button>

                            </Form>

                        </ModalBody>
                </Modal>


            </React.Fragment>
        )


   }


}


export default Header;