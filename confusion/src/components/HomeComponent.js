import React from 'react';
import {Card, CardImg, CardText,CardBody,CardTitle, CardSubtitle} from 'reactstrap';
//animations
import {FadeTransform} from 'react-animation-components';
//components
import {Loading} from './LoadingComponent';
import { baseURL} from '../shared/baseURL';

// {item.designation ? <CardSubtitle>{item.designation}</CardSubtitle>:null}= No todos los item tienen la propiedad designation (solo aplica a leaders.js ) Esta insttruccion es un if, pregunta si existe la propiedad designation, si es asi la dibuja caso contrario envia un null<


function RenderCard({item, isLoading,errMess}){

    if(isLoading){
        return(
            <Loading />

        );
    } else if(errMess){
        return(
            <h4>{errMess}</h4>
        );
    } else{
        return(
            <FadeTransform in 
            transformProps={{
                exitTransform: 'scale(0.5) translateY(-50%)'
            }}>
                    <Card>
                        <CardImg src={baseURL + item.image} alt={item.name} />
                        <CardBody>
                            <CardTitle>{item.name}</CardTitle>
                                {item.designation ? <CardSubtitle>{item.designation}</CardSubtitle>:null}
                            <CardText>{item.description}</CardText>
                        </CardBody>
                    </Card>
            </FadeTransform>
        );
    }            
}


function Home(props){
    return(
         
            <div className="container">
                 <div className="row align-items-star">
                    <div className="col-12 col-md m-1">
                        <RenderCard item={props.dish}
                                    isLoading={props.dishesLoading}
                                    errMess={props.dishesErrMess} />
                    </div>

                    <div className="col-12 col-md m-1">
                        <RenderCard item={props.promotion}
                                    isLoading={props.promosLoading}
                                    errMess={props.promosErrMess} />
                    </div>

                    <div className="col-12 col-md m-1">
                        <RenderCard item={props.leader} 
                                    isLoading={props.leaderLoading}
                                    errMess={props.leaderErrMess}/>
                    </div>
                 </div>
            </div>
    );

}


export default Home;