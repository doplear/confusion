import React, { Component } from 'react';
import { Card, CardImg, CardText, CardBody, CardTitle, Breadcrumb,BreadcrumbItem,
   Button,Modal,ModalHeader,ModalBody, Label, Row,Col } from 'reactstrap';
import {Link } from 'react-router-dom';
import {Control, LocalForm,Errors} from 'react-redux-form';

//animations
import {FadeTransform, Fade, Stagger } from 'react-animation-components';

//component
import {Loading} from './LoadingComponent';

import { baseURL} from '../shared/baseURL';

const required =(val) => val && val.length;
const maxLenght = (len) => (val) =>!(val) || (val.length<=len);
const minLenght = (len) => (val) => (val) && (val.length>=len);

class CommentForm extends Component {

  constructor(props){
    super(props);
    this.state={
      isModalOpen:false
     }   
     this.toggleModal = this.toggleModal.bind(this);     
     this.handleSubmit = this.handleSubmit.bind(this);
     
  }

  toggleModal(){
    this.setState({
        isModalOpen:!this.state.isModalOpen
    });
}


handleSubmit(values){
 this.toggleModal();
 this.props.postComment(this.props.dishId,values.rating,values.author,values.comment);

}




render(){
return( 
  <div>
    <Button  outline onClick={this.toggleModal} color="primary" >
      <span className="fa fa-pencil"></span> Submit Comments
    </Button>

    <Modal isOpen={this.state.isModalOpen} toggle={this.toggleModal}>
              <ModalHeader toggle={this.toggleModal}>Submit Comment</ModalHeader>
              <ModalBody>
                  <LocalForm onSubmit={(value) =>{this.handleSubmit(value)}}>
                      <Row className="form-group">
                          <Label htmlFor="rating" md={2}>Rating</Label>
                            <Col md={10}>
                                <Control.select model=".rating"  id="rating" name="rating" placeholder="Rating" className="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                        <option>3</option>
                                        <option>4</option>
                                        <option>5</option>
                                </Control.select>                                      
                            </Col>
                        </Row>

                        <Row className="form-group">
                            <Label htmlFor="yourname" md={2}>Your Name</Label>
                            <Col md={10}>
                                <Control.text model=".author"  id="author" name="author" 
                                    placeholder="Your Name" 
                                    className="form-control"
                                    validators={{
                                        required,
                                        minLenght:minLenght(3),
                                        maxLenght:maxLenght(15)                                  
                                    } }                            
                                /> 
                                <Errors  
                                    className="text-danger"
                                    model=".author"
                                    show="touched"
                                    messages={{
                                        required:'Required',
                                        minLenght:'Must be greater than 2 characters',
                                        maxLenght:'Must be 15 characters or less'
                                      
                                    }}/>
                                                                      
                            </Col>
                        </Row>  
                        <Row className="form-group">
                                    <Label htmlFor="comment" md={2}>Your Feedback</Label>
                                    <Col md={10}>
                                        <Control.textarea model=".comment" id="comment" name="comment"
                                            rows="6" 
                                            className="form-control"                                            
                                          >
                                        </Control.textarea>
                                    </Col>
                          </Row>
                          <Button type="submit" value="submit" color="primary">Submit</Button>
                  </LocalForm>
              </ModalBody>
      </Modal>

  </div>);

}

}


function RenderDish({dish}){
  
    return(
      <div className="col-12 col-md-5 m-1">
          <FadeTransform in 
            transformProps={{
                exitTransform: 'scale(0.5) translateY(-50%)'
            }}>
            <Card>
              <CardImg width="100%" src={baseURL + dish.image} alt={dish.name} />
                <CardBody>
                  <CardTitle>{dish.name}</CardTitle>
                      <CardText>{dish.description}</CardText>
                </CardBody> 
            </Card>
        </FadeTransform>
      </div>
    ); 
}


function RenderComments({comments, postComment,dishId}) {
  if (comments) {
    return (
      <div className ="col-12 col-md-5 m-1" >                            
              <h4>Comments</h4>
              <ul className ="list-unstyled" >
                <Stagger in>
                    {comments.map(item => {
                      return(
                          <Fade>
                            <li key={item.id} >
                              <p>{item.comment} </p> 
                              <p>-- {item.author} , {new Intl.DateTimeFormat('en-US',{year:'numeric', month:'short',day:'2-digit'}).format(new Date(Date.parse(item.date)))}</p> 
                            </li>
                          </Fade>
                          ) 
                      })}
                  </Stagger>
              </ul>  
              <CommentForm dishId={dishId} postComment={postComment}/>             
      </div> 
    )
  } else {
    return <div></div>
  }

};



const DishDetail = (props) => {
  if(props.isLoading){
    return(

      <div className="container">
          <div className="row">
              <Loading />
          </div>
      </div>
    );
  } else if(props.errMess){
    return(

      <div className="container">
          <div className="row">
              <h4>{props.errMess}</h4>
          </div>
      </div>
    );

  } 
    else  if (props.dish != null) {  
        return (
            <div className="container">
              <div className="row">
                  <Breadcrumb>
                    <BreadcrumbItem><Link to='/menu'>Menu</Link></BreadcrumbItem>
                    <BreadcrumbItem active>{props.dish.name}</BreadcrumbItem>
                  </Breadcrumb>
                    <div className="col-12">
                        <h3>{props.dish.name}</h3>
                        <hr />
                    </div>
                </div>
                  <div className="row"> 
                        <RenderDish dish={props.dish} />
                        <RenderComments comments= {props.comments}
                          postComment={props.postComment}
                          dishId={props.dish.id} />                    
                  </div>   
                  
                  >
                  
            </div> 
        );

      } else {

        return (<div></div>)
      }
    }








export default DishDetail;