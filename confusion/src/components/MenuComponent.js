
import React from 'react'
import { Card, CardImg,CardImgOverlay,CardTitle, Breadcrumb,BreadcrumbItem  } from 'reactstrap';
import { Link } from 'react-router-dom';

//components
import {Loading} from './LoadingComponent';

import { baseURL} from '../shared/baseURL';

//src={baseURL+dish.image}= con esto traemos la imagen desde el server
//RenderMenuitem defino aca lo que recibo por props usando desestructuracion, tambien podria poner RenderMenuitem(props)
//aca implemtentamos functional component en lugar de implemntar un class component
//lo hacemos debido a que este componente solo recibe props y no maneja estados no tiene un contruct method
function RenderMenuitem({dish}){
  return(
    <Card> 
     <Link to={`/menu/${dish.id}`}>                  
      <CardImg width="100%" src={baseURL+dish.image} alt={dish.name} />                  
        <CardImgOverlay>
          <CardTitle>{dish.name}</CardTitle>
        </CardImgOverlay>
    </Link>
  </Card>

  );


}
  //es necesario cada vez que se crea un objeto definir un constructor
  //state = estado inicial del objeto y en este caso se define una propiedad 
  //disher que se usara dentro de render(). Se utiliza state para guardar valores

  

  //siempre es necesario utilizar render para que se dibuje el diseño al usuario
  //key={dish.id} es necesario porque en react cada vez que se construye una lista de items en React
  //cada item requiere un atributo key ya que react utiliza ese id para identificar univocamente a cada elemnto
  // en caso de que se necesite actulizar la UI
 // se cambia this.state.dishes a this.props.dishes ya que ahora obtenemos la info de props y no desde states
 //this.props.onClick() como desde el MainComponent paso el evento onClic en el evento on clic del aca tengo que invocarlo via props  
/*
en props estoy recibiendo la informacion asi, por eso para leer dishes tengo que hacer props.dishes.dishes: 
  props = {dishes{
                dishes[],
                isLoading,
                errMess: null
    }
    
  }
*/
 const Menu = (props) =>{  
 const menu =  props.dishes.dishes.map((dish) => {
            return (
              <div key={dish.id} className="col-12 col-md-5 m-1">
              <RenderMenuitem dish={dish}  />
              </div>
            );
          });

//{this.renderDish(this.state.selectedDish)} con esto dibuja la descripcion de la imagen seleccionada
//{menu} tiene la definicion de menu que se hace arriba en const menu = this.props.dishes.map((dish)

      if(props.dishes.isLoading){        
        return(
          <div className="container">
              <div className="row">
                  <Loading />
              </div>
          </div>
        );
      } else if(props.dishes.errMess){
          return(    
            <div className="container">
                <div className="row">
                    <h4>{props.dishes.errMess}</h4>
                </div>
            </div>
          );    
      } else{          
          return (
            <div className="container">
              <div className="row">
                <Breadcrumb>
                  <BreadcrumbItem><Link to='/home'>Home</Link></BreadcrumbItem>
                  <BreadcrumbItem active>Menu</BreadcrumbItem>
                </Breadcrumb>
                  <div className="col-12">
                      <h3>Menu</h3>
                      <hr />
                  </div>
              </div>
              <div className="row">
                    {menu}
              </div>          
            </div>
          );
        }

  
      }
    export default Menu;
