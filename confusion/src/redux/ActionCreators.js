

//aca vamos a importar todas las acciones definidas en ActionTypes
//estas acciones se envian al store
import * as ActionTypes from './ActionTypes';

import {baseURL} from '../shared/baseURL';


//this is a function that created action object
//el formato del objeto que esta devolviendo esta funcion (type y payload) es el estandar de redux
//definimos que va a contener addComment
//addComment devuelve una accion
export const addComment = (comment) =>({
        //aca se asigna a type el valor de ADD_COMMENT que trae de ActionTypes
        type: ActionTypes.ADD_COMMENT,
        payload: comment
});

export const postComment =  (dishId,rating,author,comment) => (dispatch) =>{

        const newComment = {
                dishId: dishId,
                rating: rating,
                author: author,
                comment: comment
        }
        newComment.date = new Date().toISOString();

        //hacemos un post. Cuanod no se espeficia el metodo por defecto es GET
        //JSON.stringify(newComment) = paso el objeto que cree como un JSON
        //'Content-Type':'application/JSON'= especifico en el header que el contenido es un JSON
        return fetch(baseURL + 'comments', {
                method: 'POST',
                body :JSON.stringify(newComment),
                headers: {
                        'Content-Type':'application/JSON'
                },
                credentials: 'same-origin'
        })

        .then(response =>{
                if(response.ok){
                    return response    
                }else{
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response=response;
                throw error;
                }
        },      
        error=>{
                var errmess = new Error(error.message);
                throw errmess;
        } )
                .then(response => response.json())
                .then(response => dispatch(addComment(response)))
                .catch(error => {console.log('Post Comments' , error.message)
                        alert('you comment could not be posted \nError:' +error.message)
        });
        
}


//fetchDishes se va a crear como un thunk por eso se define como un arrow function. 
//Thunk es una funcion que devuelve una funcion
//thunk es un midleware, se va a ejecutar en este caso mientras se monta el objeto main
export const fetchDishes = ()=> (dispatch) =>{
        dispatch(dishesLoading(true));
        
        //la respuesta del server que me da el response.json lo pasamos dishes en el siguiente .then
        //si response.ok entonces pasa los datos al siguiente .then porque estan encadenados
        return fetch(baseURL + 'dishes')
        .then(response =>{
                if(response.ok){
                    return response    
                }else{
                //response.status tiene el codigo de error 
                //response.statusTex tiene el mensaje de error     
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response=response;
                //si ocurre el error dentro de la promise el flujo ira hacia el catch definido abajo<
                throw error;
                }
        },
        //en caso de que el server no responda el flujo entra aca donde en la promesa definimos el error handler
        error=>{
                //cuando ocurre un error error.message va a tener ese mensaje
                var errmes = new Error(error.message)
                throw errmes;
                //el flujo se dirige al catch definido abajo
        } )
        .then(response => response.json())
        .then(dishes => dispatch(addDishes(dishes)))
        .catch(error => dispatch(dishesFailed(error.message)));


}

//dishesLoading devuelve una objeto accion
export const dishesLoading = () =>({
        type: ActionTypes.DISHES_LOADING
});


export const dishesFailed = (errmess) =>({
        type: ActionTypes.DISHES_FAILED,
        payload: errmess

});


export const addDishes =(dishes) =>({
        type: ActionTypes.ADD_DISHES,
        payload: dishes
})


export const fetchComments = ()=> (dispatch) =>{      

        return fetch(baseURL + 'comments')
        .then(response =>{
                if(response.ok){
                    return response    
                }else{
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response=response;
                throw error;
                }
        },      
        error=>{
                var errmes = new Error(error.message)
                throw errmes;
        } )
                .then(response => response.json())
                .then(comments => dispatch(addComments(comments)))
                .catch(error => dispatch(commentsFailed(error.message)));

}

export const commentsFailed = (errmess) =>({
        type: ActionTypes.COMMENTS_FAILED,
        payload: errmess

});


export const addComments =(comments) =>({
        type: ActionTypes.ADD_COMMENTS,
        payload: comments



})



export const fetchPromos = ()=> (dispatch) =>{
        dispatch(promosLoading(true));
        return fetch(baseURL + 'promotions')
        .then(response =>{
                if(response.ok){
                    return response    
                }else{
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response=response;
                throw error;
                }
        },      
        error=>{
                var errmes = new Error(error.message)
                throw errmes;
        } )
                //la respuesta del server que me da el response.json lo pasamos dishes en el siguiente .then
                .then(response => response.json())
                .then(promos => dispatch(addPromos(promos)))
                .catch(error => dispatch(promosFailed(error.message)));



}

//dishesLoading devuelve una objeto accion
export const promosLoading = () =>({
        type: ActionTypes.PROMOS_LOADING
});


export const promosFailed = (errmess) =>({
        type: ActionTypes.PROMOS_FAILED,
        payload: errmess

});


export const addPromos =(promos) =>({
        type: ActionTypes.ADD_PROMOS,
        payload: promos
})


//LIDERS

export const fetchLiders = ()=> (dispatch) =>{      
        dispatch(lidersLoading(true));
        return fetch(baseURL + 'leaders')
        .then(response =>{
                if(response.ok){
                   
                    return response    
                }else{
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response=response;
                throw error;
                }
        },      
        error=>{
                var errmes = new Error(error.message)
                throw errmes;
        } )
                .then(response => response.json())
                .then(leaders => dispatch(addLeaders(leaders)))
                .catch(error => dispatch(leadersFailed(error.message)));

}




export const addLeaders =(leaders) =>({
        type: ActionTypes.ADD_LEADERS,
        payload: leaders
})

export const leadersFailed = (errmess) =>({
        type: ActionTypes.LEADERS_FAILED,
        payload: errmess

});

export const lidersLoading = () =>({
        type: ActionTypes.LEADERS_LOADING
});


//FEEDBACK


export const postFeedback =  (firstname,lastname,telnum,email,agree,contactType,message) => (dispatch) =>{

        const newFeedback = {
                firstname: firstname,
                lastname: lastname,
                telnum: telnum,
                email: email,
                agree: agree,
                contactType: contactType,
                message:message
        }
        newFeedback.date = new Date().toISOString();

        return fetch(baseURL + 'feedback', {
                method: 'POST',
                body :JSON.stringify(newFeedback),
                headers: {
                        'Content-Type':'application/JSON'
                },
                credentials: 'same-origin'
        })

        .then(response =>{
                if(response.ok){
                    return response    
                }else{
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response=response;
                throw error;
                }
        },      
        error=>{
                var errmess = new Error(error.message);
                throw errmess;
        } )
                .then(response => response.json())
                .then(response => dispatch(addFeedback(response)))
                .catch(error => {console.log('Post feedback' , error.message)
                        alert('you feedback could not be posted \nError:' +error.message)
        });
        
}


export const addFeedback = (feedback) =>({
        //aca se asigna a type el valor de ADD_COMMENT que trae de ActionTypes
        type: ActionTypes.ADD_FEEDBACK,
        payload: feedback
});

