
//combineReducers= como tenemos los reducers separados la forma de combinarlos es utilizar este metodo de redux
import  { createStore, combineReducers,applyMiddleware } from 'redux'

//createForms=permite=nos permite agregar el estatdo de form en nuestro store
import {createForms} from 'react-redux-form';
import thunk from 'redux-thunk';
import logger from 'redux-logger'

//importo los datos de cada reduce
import { Dishes  } from './dishes';
import { Comments  } from './comments';
import { Leaders  } from './leaders';
import { Promotions  } from './promotions';

import { InitialFeedback } from './forms';
//creacion de Store en Redux
//ConfigureStore=la idea de esta funcion es que cuando la llamen se cree el store utilizando la funcion createStore de redux

export const ConfigureStore =() =>{
//createStore acepta otro parametro que es enhacer ahi pasamos applyMiddleware(thunk,logger)
//logger me permite ver en la consola de la pagina web los distintos estados por los que pasan mis redux stores 
 const store = createStore(
     //aca vamos a definir el estado global de nuestra aplicacion como una combinacion de todos los reducers
     //  ...createForms con esto vamos a guardar el estado del form Contact y si navegamos a otro link lo que completamos en ese form permanecera ahi ya que quedo guardado en el store
        combineReducers({
            dishes: Dishes,
            comments:Comments,
            promotions:Promotions,
            leaders:Leaders,          
            ...createForms({
                feedback:InitialFeedback
            })
        }),
        applyMiddleware(thunk,logger)
    
    );
//se usa en MainCoponents
    return store;
}