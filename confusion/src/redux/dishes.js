
//aca tambien tenemos que importar los ActionTypes
import * as ActionTypes from './ActionTypes';
//cada uno de los reducers recibe un state y un action


//  isLoading:true,errMess: null,dishes:[] Se colocan estos valores porque en principio no se tiene la informacion
//de dishes por eso corresponde [], tampoco hay error por eso errMess=null y como se esta intentando aun obtener 
//la informacion de dishes isLoading = true

export const Dishes = (state = {
        isLoading:true, 
        errMess: null,
        dishes:[]
}, action)=> {

        switch(action.type){
                case ActionTypes.ADD_DISHES:
                        return {...state,isLoading:false,errMess:null,dishes: action.payload}     

                case ActionTypes.DISHES_LOADING:
                        return {...state,isLoading:true,errMess:null,dishes:[]}
                
                case ActionTypes.DISHES_FAILED:
                        return {...state,isLoading:false,errMess:action.payload,dishes:[]}
                        
            default:
                return state;

        }


}