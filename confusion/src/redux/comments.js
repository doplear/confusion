
//aca tambien tenemos que importar los ActionTypes
import * as ActionTypes from './ActionTypes';

//cada uno de los reducers recibe un state y un action
//

export const Comments = (state = {
        errMess: null,
        comments: []
        }, action)=> {
        
        switch(action.type){
                case ActionTypes.ADD_COMMENTS:
                        return {...state,isLoading:false,errMess:null,comments: action.payload} 
                case ActionTypes.COMMENTS_FAILED:
                        return {...state,isLoading:false,errMess:action.payload,comments:[]}
        
                // ActionTypes.ADD_COMMENT='ADD_COMMENT'
                case ActionTypes.ADD_COMMENT:
                        var comment = action.payload;
                        //por reglas de redux, no se puede modificar el state que se envio por parametro
                        //puedo agregar informacion, por eso usamos concat que creara un nuevo objeto
                        //concat is an inmutable operation
                        //El método concat() se usa para unir dos o más arrays. Este método no cambia los arrays existentes, sino que devuelve un nuevo array.
                        //https://developer.mozilla.org/es/docs/Web/JavaScript/Referencia/Objetos_globales/Array/concat
                        return {...state, comments: state.comments.concat(comment)};
                default:
                        return state;
        


        }


}